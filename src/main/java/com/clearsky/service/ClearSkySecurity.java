package com.clearsky.service;

import com.clearsky.enums.ActionAllowed;
import com.clearsky.model.ClearSkyUserInfo;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.stream.Collectors;

@Service("CSSecurity")
public class ClearSkySecurity {

    //TODO: Create auth for orgId with projectId

    public boolean auth(Authentication authentication, int orgId, int actionAllowed, String... securityEntity){
        //key is a security entity, value is a action allowed
        Map<String, Integer> permissions = authentication.getAuthorities()
                .stream().collect(Collectors.toMap(ga -> ga.getAuthority().split("_")[0],
                                                   ga -> Integer.valueOf(ga.getAuthority().split("_")[1])));
        boolean hasPermission = false;
        for (String entity : securityEntity) {
            if (permissions.containsKey(entity) && (permissions.get(entity) & actionAllowed) > 0)
                hasPermission = true;
        }

        return hasPermission && auth(authentication, orgId);
    }


    public boolean auth(Authentication authentication, int orgId){
        ClearSkyUserInfo user = (ClearSkyUserInfo) authentication.getPrincipal();

        if (user.getOrgId() == 0)
            return true;

        return user.getOrgId() == orgId;
    }
}
