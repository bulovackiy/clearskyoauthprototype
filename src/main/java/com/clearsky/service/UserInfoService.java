package com.clearsky.service;

import com.clearsky.exceptions.UserNotFoundException;
import com.clearsky.model.ClearSkyUserInfo;
import com.clearsky.persistance.entity.ClearSkyUser;
import com.clearsky.persistance.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserInfoService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        User user = (User) authentication.getPrincipal();

        final ClearSkyUser clearSkyUser = userRepository.findByEmailAndOrganization_SiteName(s, user.getUsername());

        if (clearSkyUser == null) {
            throw new UserNotFoundException(s);
        }

        return new ClearSkyUserInfo(clearSkyUser);
    }
}
