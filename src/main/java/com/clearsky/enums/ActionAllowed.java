package com.clearsky.enums;

import org.springframework.stereotype.Component;

@Component
public class ActionAllowed {

    public final static int CREATE = 1;
    public final static int READ = 2;
    public final static int EDIT = 4;
    public final static int DELETE = 8;
    public final static int GENERATE = 16;
    public final static int RUN = 32;
    public final static int PRIMARY = 64;
}
