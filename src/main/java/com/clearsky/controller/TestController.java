package com.clearsky.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class TestController {


    @RequestMapping(value = "/api/", method = RequestMethod.GET)
    public String getAllUsers(){
        return "it is 'api'";
    }

    @RequestMapping(value = "/api/organization", method = RequestMethod.GET)
    public String getAllUsersHello(){
        return "it is 'organization'";
    }

    @RequestMapping(value = "/api/organization/{orgId}/roles", method = RequestMethod.GET)
    @PreAuthorize("@CSSecurity.auth(authentication, #orgId, @actionAllowed.CREATE, " +
            "'Admin::Users::Global', 'Admin::Users::Site')")
    public String getUser(@PathVariable("orgId") int orgId){
        return "it is a '/api/organization/" + orgId + "/roles'";
    }

    @RequestMapping(value = "/api/organization/{orgId}/usergroup", method = RequestMethod.POST)
    @PreAuthorize("@CSSecurity.auth(authentication, #orgId, @actionAllowed.CREATE, " +
            "'Admin::Users::Global')")
    public String getUserGroup(@PathVariable("orgId") int orgId){
        return "it is a '/api/organization/" + orgId + "/usergroup'";
    }

    @RequestMapping(value = "/api/organization/{orgId}/project/{projectId}", method = RequestMethod.GET)
    public String deleteAllUsers(@PathVariable("orgId") int orgId, @PathVariable("projectId") int projectId){
        return String.format("it is a '/api/organization/%1s/project/%2s'", orgId, projectId);
    }
}
