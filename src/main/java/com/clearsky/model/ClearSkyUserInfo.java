package com.clearsky.model;

import com.clearsky.auth.model.UserInfo;
import com.clearsky.persistance.entity.ClearSkyUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ClearSkyUserInfo implements UserInfo {

    private final ClearSkyUser clearSkyUser;

    public ClearSkyUserInfo(ClearSkyUser clearSkyUser) {
        this.clearSkyUser = clearSkyUser;
    }

    @Override
    public String getUsername() {
        return clearSkyUser.getEmail();
    }

    @Override
    public String getPassword() {
        return clearSkyUser.getPassword();
    }

    public Long getId() {
        return clearSkyUser.getUserId();
    }

    public Integer getUserGroup() {
        return clearSkyUser.getUserGroup().getId();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        final List<GrantedAuthority> authorities = new ArrayList<>();

        clearSkyUser.getUserInRoles().getRole().getSecurityRolePermissions()
                .forEach(srp -> authorities.add(new SimpleGrantedAuthority(srp.getEntity().getName() + "_"
                        + srp.getAllowedAction())));

        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return clearSkyUser.getActive();
    }

    public String getFirstName() {
        return clearSkyUser.getFirstname();
    }

    public String getLastName() {
        return clearSkyUser.getLastname();
    }

    public String getPhoneNumber() {
        return clearSkyUser.getPhoneNo();
    }

    public boolean isDefaultImageUsed() {
        return clearSkyUser.getIsDefaultImageUse();
    }

    public String getTimeZone() {
        return clearSkyUser.getUsertimezone();
    }

    @Override
    public Integer getOrgId() {
        return clearSkyUser.getOrganization().getId();
    }

    public String getOrgName() {
        return clearSkyUser.getOrganization().getName();
    }

    public String getSiteName() {
        return clearSkyUser.getOrganization().getSiteName();
    }
}
