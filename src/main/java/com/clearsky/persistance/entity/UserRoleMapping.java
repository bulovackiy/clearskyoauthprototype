package com.clearsky.persistance.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user_security_role")
public class UserRoleMapping implements Serializable {

    public UserRoleMapping() {
        super();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "user_id")
    private int user;

    @Column(name = "org_id")
    private int orgId;

    @Column(name = "project_id")
    private Integer projectId;

    @ManyToOne
    @JoinColumn(name = "security_role_id", referencedColumnName = "id")
    private Role role;

    public int getId() {
        return id;
    }

    public int getUser() {
        return user;
    }

    public int getOrgId() {
        return orgId;
    }

    public int getProjectId() {
        return projectId;
    }

    public Role getRole() {
        return role;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
