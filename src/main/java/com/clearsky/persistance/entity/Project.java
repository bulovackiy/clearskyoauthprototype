package com.clearsky.persistance.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "project")
public class Project {

    public Project() {
        super();
    }

    @Id
    private int id;

    private String name;

    @Column(name = "org_id")
    private int orgId;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @JsonIgnore
    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return id == project.id &&
          orgId == project.orgId &&
          Objects.equals(name, project.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, orgId);
    }

    @Override
    public String toString() {
        return "Project{" +
          "id=" + id +
          ", name='" + name + '\'' +
          ", orgId=" + orgId +
          '}';
    }
}
