package com.clearsky.persistance.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "userdetail")
public class ClearSkyUser implements Serializable {

    public ClearSkyUser() {
        super();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "userid", columnDefinition="serial", insertable = false, updatable = false)
    private Long userId;

    @Column(nullable = false, unique = true)
    private String email;

    @JsonIgnore
    private String password;

    private String firstname;

    private String lastname;

    @Column(name = "isactive")
    @JsonIgnore
    private Boolean isActive;

    @Column(name = "confirmation_token")
    @JsonIgnore
    private String confirmationToken;

    @Column(name="phonenumber1")
    private String phoneNo;

    private String profilephoto;

    private String usertimezone;

    @Column(name = "isdefaultimageuse")
    private Boolean isDefaultImageUse;

    @Column(name = "ispasswordset")
    @JsonIgnore
    private Boolean isPasswordSet;

    @Column(name = "jobtitle")
    private String jobTitle;
    @ManyToOne
    @JoinColumn(name = "org_id", referencedColumnName = "id")
    private Organization organization;

    @ManyToOne
    @JoinColumn(name = "userid", referencedColumnName = "user_id")
    private UserRoleMapping userInRoles;

    @ManyToOne
    @JoinColumn(name = "user_group_id", referencedColumnName = "id")
    private UserGroup userGroup;

    public UserGroup getUserGroup() {
        return userGroup;
    }

    public Organization getOrganization() {
        return organization;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Long getUserId() {
        return userId;
    }

    public Boolean getActive() {
        return isActive;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getProfilephoto() {
        return profilephoto;
    }

    public Boolean getIsDefaultImageUse() {
        return isDefaultImageUse;
    }

    public Boolean getPasswordSet() {
        return isPasswordSet;
    }

    public String getUsertimezone() {
        return usertimezone;
    }

    public Boolean getDefaultImageUse() {
        return isDefaultImageUse;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPasswordSet(Boolean passwordSet) {
        isPasswordSet = passwordSet;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public void setProfilephoto(String profilephoto) {
        this.profilephoto = profilephoto;
    }

    public void setUsertimezone(String usertimezone) {
        this.usertimezone = usertimezone;
    }

    public void setDefaultImageUse(Boolean defaultImageUse) {
        isDefaultImageUse = defaultImageUse;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public void setUserInRoles(UserRoleMapping userInRoles) {
        this.userInRoles = userInRoles;
    }

    public void setUserGroup(UserGroup userGroup) {
        this.userGroup = userGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClearSkyUser clearSkyUser = (ClearSkyUser) o;
        return Objects.equals(id, clearSkyUser.id) &&
                Objects.equals(userId, clearSkyUser.userId) &&
                Objects.equals(email, clearSkyUser.email) &&
                Objects.equals(password, clearSkyUser.password) &&
                Objects.equals(firstname, clearSkyUser.firstname) &&
                Objects.equals(lastname, clearSkyUser.lastname) &&
                Objects.equals(isActive, clearSkyUser.isActive) &&
                Objects.equals(confirmationToken, clearSkyUser.confirmationToken) &&
                Objects.equals(phoneNo, clearSkyUser.phoneNo) &&
                Objects.equals(profilephoto, clearSkyUser.profilephoto) &&
                Objects.equals(usertimezone, clearSkyUser.usertimezone) &&
                Objects.equals(isDefaultImageUse, clearSkyUser.isDefaultImageUse) &&
                Objects.equals(isPasswordSet, clearSkyUser.isPasswordSet) &&
                Objects.equals(jobTitle, clearSkyUser.jobTitle) &&
                Objects.equals(organization, clearSkyUser.organization) &&
                Objects.equals(userInRoles, clearSkyUser.userInRoles) &&
                Objects.equals(userGroup, clearSkyUser.userGroup);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, userId, email, password, firstname, lastname, isActive, confirmationToken, phoneNo, profilephoto, usertimezone, isDefaultImageUse, isPasswordSet, jobTitle/*, organization, userInRoles, userGroup*/);
    }
    public UserRoleMapping getUserInRoles() {
        return userInRoles;
    }
}
