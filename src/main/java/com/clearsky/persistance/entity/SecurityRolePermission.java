package com.clearsky.persistance.entity;


import javax.persistence.*;

@Entity
@Table(name = "security_role_permission")
public class SecurityRolePermission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "security_entity_id", referencedColumnName = "id")
    private SecurityEntity entity;

    @Column(name = "allowed_action")
    private int allowedAction;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAllowedAction() {
        return allowedAction;
    }

    public void setAllowedAction(int allowedAction) {
        this.allowedAction = allowedAction;
    }

    public SecurityEntity getEntity() {
        return entity;
    }

    public void setEntity(SecurityEntity entity) {
        this.entity = entity;
    }
}
