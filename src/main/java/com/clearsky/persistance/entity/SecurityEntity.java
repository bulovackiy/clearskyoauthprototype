package com.clearsky.persistance.entity;


import javax.persistence.*;

@Entity
@Table(name = "security_entity")
public class SecurityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
