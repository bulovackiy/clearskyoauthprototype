package com.clearsky.persistance.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "organization")
public class Organization {

    public Organization() {
        super();
    }

    public Organization(String name, String siteName, String timezone) {
        this.name = name;
        this.siteName = siteName;
        this.timezone = timezone;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "isactive")
    private Boolean isActive;


    @Column(name = "isdeleted")
    private Boolean isDeleted;

    private String name;

    @Column(name = "sitename")
    private String siteName;

    private String timezone;

    @JsonIgnore
    private boolean internal;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Boolean getActive() {
        return isActive;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public String getSiteName() {
        return siteName;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public boolean isInternal() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Organization that = (Organization) o;
        return internal == that.internal &&
          Objects.equals(isActive, that.isActive) &&
          Objects.equals(isDeleted, that.isDeleted) &&
          Objects.equals(name, that.name) &&
          Objects.equals(siteName, that.siteName) &&
          Objects.equals(timezone, that.timezone);
    }

    @Override
    public int hashCode() {

        return Objects.hash(isActive, isDeleted, name, siteName, timezone, internal);
    }
}
