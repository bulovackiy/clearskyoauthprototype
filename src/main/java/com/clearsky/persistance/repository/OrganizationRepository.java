package com.clearsky.persistance.repository;

import com.clearsky.persistance.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrganizationRepository extends JpaRepository<Organization, Integer> {

    Organization findBySiteName(final String siteName);

    Organization findBySiteNameAndIsActiveAndIsDeleted(final String siteName, final Boolean isActive,
                                                       final Boolean isDeleted);

    List<Organization> findByIsDeletedAndIsActiveAndInternal(final Boolean isDeleted, final Boolean isActive,
                                                             final Boolean internal);

    List<Organization> findByInternal(boolean internal);
}