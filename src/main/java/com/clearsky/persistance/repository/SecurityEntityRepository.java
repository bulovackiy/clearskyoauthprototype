package com.clearsky.persistance.repository;

import com.clearsky.persistance.entity.SecurityEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SecurityEntityRepository extends JpaRepository<SecurityEntity, Integer> {

    SecurityEntity findByName(final String name);
}