package com.clearsky.persistance.repository;

import com.clearsky.persistance.entity.ClearSkyUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<ClearSkyUser, Long> {

    ClearSkyUser findByEmailAndOrganization_SiteName(final String email, final String siteName);

    ClearSkyUser findByUserId(final Long userId);

    ClearSkyUser findByConfirmationToken(final String token);

    @Transactional
    @Procedure("ssp_getpermissionstring")
    String getPermissionString(Long userId, String jwt, Integer orgId);

    @Transactional
    @Procedure("ssp_getpermissionstringforadmin")
    String getPermissionString(Long userId, String _jwt);

    @Transactional
    @Procedure("ssp_getuserorgpermissions")
    String getPermissionObject(Long userId, Integer orgId);

    @Transactional
    @Procedure("ssp_userbelongstoorganization")
    Boolean checkUserBelongsToOrganization(Long userId, String _orgName);

    @Transactional
    @Procedure("ssp_adduser")
    Long addUser(String email, String firstName, String lastName,
                 String timeZone, String phone, String job, Integer group,
                 String token, Integer orgId);

    @Transactional
    @Procedure("ssp_assignusertorole")
    Long assignUserToRole(Long userId, Integer orgId, Integer projectId, Integer roleId);

    @Transactional
    @Procedure("ssp_isusersuperadmin")
    Boolean checkUserIsSuperAdmin(Long userId);

    @Transactional
    @Procedure("ssp_isuserprimaryadmin")
    Boolean checkUserIsPrimarySiteAdmin(Long userId, Integer orgId);

    @Transactional
    @Procedure("ssp_deleteuser")
    Integer deleteUser(Long id);

    @Transactional
    @Procedure("ssp_clearassignmentsinorganization")
    Long clearAllAssignment(Long userId, Integer orgId);

    @Transactional
    @Query(nativeQuery = true, value = "SELECT * FROM checkifuserscanberemoved(:ids)")
    List<ClearSkyUser> checkIfUserCanBeRemoved(@Param("ids") String ids);

    @Transactional
    @Query(nativeQuery = true, value = "SELECT * FROM getusersnotbelongtoorg(:userIds, :orgId)")
    List<ClearSkyUser> getUsersNotBelongToOrg(@Param("userIds") String userIds, @Param("orgId") int orgId);
}