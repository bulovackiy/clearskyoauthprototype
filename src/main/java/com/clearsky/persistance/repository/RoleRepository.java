package com.clearsky.persistance.repository;

import com.clearsky.persistance.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    List<Role> findByOrgId(final int orgId);

    List<Role> findByOrgIdAndGroupId(final int orgId, final int groupId);

    @Transactional
    @Procedure("addprimarysiteadminroletoorganization")
    Integer addPrimarySiteAdminToOrganization(Integer orgId);

    @Transactional
    @Procedure("addsiteadminroletoorganization")
    Integer addSiteAdminToOrganization(Integer orgId);

    @Transactional
    @Procedure("adddefaultuserrolestoorganization")
    Boolean addDefaultUserRolesToOrganization(Integer orgId);

    @Transactional
    @Procedure("finduserswithroles")
    String findUsersWithRoles(String roleIds);

    @Transactional
    @Procedure("ssp_deleterole")
    Integer deleteRole(Integer id);
}