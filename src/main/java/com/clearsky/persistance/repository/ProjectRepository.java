package com.clearsky.persistance.repository;

import com.clearsky.persistance.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {

    Project findByIdAndOrgId(final int id, final int orgId);

    List<Project> findByOrgId(final int orgId);
}