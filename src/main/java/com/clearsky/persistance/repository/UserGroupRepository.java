package com.clearsky.persistance.repository;

import com.clearsky.persistance.entity.UserGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserGroupRepository extends JpaRepository<UserGroup, Integer> {

}