package com.clearsky.component;

import com.clearsky.model.ClearSkyUserInfo;
import com.clearsky.persistance.entity.ClearSkyUser;
import com.clearsky.persistance.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ClearSkyTokenEnhancer implements TokenEnhancer {

    @Autowired
    private UserRepository userRepository;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        ClearSkyUserInfo clearSkyUser = (ClearSkyUserInfo) authentication.getPrincipal();

        String permissionString = "";
        if (clearSkyUser.getOrgId() == 0)
            permissionString = userRepository.getPermissionString(clearSkyUser.getId(), accessToken.getValue());
        else
            permissionString = userRepository.getPermissionString(clearSkyUser.getId(), accessToken.getValue(), clearSkyUser.getOrgId());

        final Map<String, Object> additionalInfo = new HashMap<>();

        additionalInfo.put("UserId", clearSkyUser.getId());
        additionalInfo.put("UserName", clearSkyUser.getUsername());
        additionalInfo.put("FirstName", clearSkyUser.getFirstName());
        additionalInfo.put("LastName", clearSkyUser.getLastName());
        additionalInfo.put("UserGroup", clearSkyUser.getUserGroup());
        additionalInfo.put("UserTimeZone", clearSkyUser.getTimeZone());
        additionalInfo.put("LoginSecret", accessToken.getValue());
        additionalInfo.put("OrganizationId", clearSkyUser.getOrgId());
        additionalInfo.put("OrganizationName", clearSkyUser.getOrgName());
        additionalInfo.put("SiteName", clearSkyUser.getSiteName());
        additionalInfo.put("PermissionsString", permissionString);

        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);

        return accessToken;
    }
}
