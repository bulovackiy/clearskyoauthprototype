package com.clearsky.exceptions;

import org.springframework.security.oauth2.common.exceptions.ClientAuthenticationException;

public class UserNotFoundException extends ClientAuthenticationException {

    public UserNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }

    public UserNotFoundException(String msg) {
        super(msg);
    }

    @Override
    public String getOAuth2ErrorCode() {
        return "user_not_found";
    }
}
